/**********
* compute_window.c
*
* This program takes in a server log file and computes window data points.
*
* Output format: <elapsed time>, <window size>
*
**********/

#include <stdio.h>
#include <string.h>         // strlen();

#define BUFSIZE 1024
//#define BUFSIZE 530

int main(int argc, char *argv[]){

    // parse command line arguments;
    if(argc != 3){
        printf("usage: %s <infile> <outfile>\n", argv[0]);
        return 0;
    }
    char infile[BUFSIZE];
    strcpy(infile, argv[1]);
    char outfile[BUFSIZE];
    strcpy(outfile, argv[2]);
    printf("%s %s\n", infile, outfile);

    // open infile;
    FILE *in;
    in = fopen(infile, "r");

    // open outfile;
    FILE *out;
    out = fopen(outfile, "w");

    // get very first sys_time;
    int read_status;
    unsigned int init_sys_time;
    unsigned int sys_time, time, snd_cwnd, rtt, bytes;
    read_status = fscanf(
        in, "%u %u %u %u %u", &sys_time, &time, &snd_cwnd, &rtt, &bytes
    );
    init_sys_time = sys_time;
    //rewind(in);

    // read infile and compute datapoints for throughput;
    unsigned int time_line, win_prev, rtt_prev;
    time_line = 0;

    // time, window size
    fprintf(out, "%u %u %u\n", 
                //sys_time - init_sys_time,
                time,
                snd_cwnd,
                rtt
                );

    win_prev = snd_cwnd;
    rtt_prev = rtt;

    while(1){
        read_status = fscanf(
            in, "%u %u %u %u %u", &sys_time, &time, &snd_cwnd, &rtt, &bytes
        );
        if(read_status != EOF){
                       
            time_line = time;//sys_time - init_sys_time;

            // if we have started a new line
            if(snd_cwnd != win_prev){
                // write out stats for previous interval;
                fprintf(out, "%u %u %u\n", 
                    time_line,
                    win_prev,
                    rtt_prev
                );
                fprintf(out, "%u %u %u\n", 
                    time_line,
                    snd_cwnd,
                    rtt
                );

                win_prev = snd_cwnd;
                rtt_prev = rtt;

            }

        }
        else{
            break;
        }
    }

    // close infile, outfile;
    fclose(in);
    fclose(out);

}