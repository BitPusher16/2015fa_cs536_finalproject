

all: client server compute_throughput compute_win_rtt merge_throughputs \
compute_cwnd merge_cwnds

client: client.c
	gcc client.c -o client

server: server.c
	gcc server.c -o server

compute_throughput: compute_throughput.c
	gcc compute_throughput.c -o compute_throughput

compute_win_rtt: compute_win_rtt.c
	gcc compute_win_rtt.c -o compute_win_rtt

merge_throughputs: merge_throughputs.c
	gcc merge_throughputs.c -o merge_throughputs

compute_cwnd: compute_cwnd.c
	gcc compute_cwnd.c -o compute_cwnd

merge_cwnds: merge_cwnds.c
	gcc merge_cwnds.c -o merge_cwnds

