/**********
* client.c
*
* sends file request to server;
* receives file from server;
*   (file is discarded, not written to disk);
**********/

#include <stdio.h>          // printf();
#include <string.h>         // strlen();
#include <sys/socket.h>     // socket();
#include <netinet/in.h>     // struct sockaddr_in;
#include <netdb.h>          // gethostbyname();

#define BUFSIZE 1024
//#define BUFSIZE 530

int main(int argc, char *argv[]){

    //parse command line arguments;
    if(argc != 3){
        printf("usage: %s <server:port> <file>\n", argv[0]);
        return 0;
    }
    char server_name[BUFSIZE];
    int i;
    for(i = 0; i < (int)strlen(argv[1]); i++){
        if(argv[1][i] != ':' && argv[1][i] != '\0'){
            server_name[i] = argv[1][i];
        }
        else{
            server_name[i] = '\0';
            break;
        }
    }
    i++;
    int port = atoi(argv[1] + i);
    char file_name[BUFSIZE];
    strcpy(file_name, argv[2]);
    printf("%s %d %s %d\n", 
        server_name, port, file_name, (int)strlen(file_name)
    );

    // create socket;
    int fd;
    if((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        perror("cannot create socket");
        return 0;
    }

    // bind socket to local port;
    struct sockaddr_in clientaddr;
    memset((char *)&clientaddr, 0, sizeof(clientaddr));
    clientaddr.sin_family = AF_INET;
    clientaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    clientaddr.sin_port = htons(0);     // selects a local port at random;
    if(bind(fd, (struct sockaddr *)&clientaddr, sizeof(clientaddr)) < 0){
        perror("bind failed");
        return 0;
    }

    // look up IP address for server;
    struct hostent *he;
    he = gethostbyname(server_name);
    if(!he){
        fprintf(stderr, "could not obtain address of %s\n", server_name);
        return 0;
    }

    // fill in server address and port;
    struct sockaddr_in servaddr;
    memset((char *)&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);
    memcpy((void *)&servaddr.sin_addr, he->h_addr_list[0], he->h_length);

    // connect to server;
    if(connect(fd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0){
        perror("connect failed");
        return 0;
    }

    // send file_name to server;
    int nbytes;
    nbytes = write(fd, file_name, strlen(file_name));

    // receive file from server;
    char inbuf[BUFSIZE];
    while(1){
        nbytes = read(fd, inbuf, BUFSIZE);
        if(nbytes > 0){
            //printf("%s", inbuf);
            //printf("received %d bytes\n", nbytes);
        }
        else{
            // receiving 0 bytes from server indicates server initiated close;
            printf("received entire file\n");
            break;
        }
    }


    // close the socket...
    close(fd);

    return 0;
}
