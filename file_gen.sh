#!/bin/bash

# usage:
# ./file_gen x_hundred_bytes outfile

STOP=${1:-4}
FILE=${2:-tmp.txt}
> $FILE
I=0
while [ $I -lt $STOP ]; 
do
printf "%09d text text text text text text text text text text text text text text text text text text\n" $(( $I * 100 )) >> $FILE
let I=I+1
done
