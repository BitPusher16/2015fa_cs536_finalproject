# Gnuplot script for comparing Reno and Vegas window fairness;
# plot_cwnds.p

set term png
set output "plot_cwnds.png"

plot "merged_cwnds.log" using 2:3 title 'reno-vegas' with linespoints