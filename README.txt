/******************************
* Tatiana Kuznetsova,
* Checed Aaron Rodgers
*
* Purdue University
* FA 2015
* CS 536 - Computer Networks
* Dr. Sonya Fahmy
*
* Final Project
******************************/

The goal of this project is to investigate the performance of the TCP Reno and TCP Vegas protocols when we run them on a network concurrently. We will use Mininet for our network simulations.
