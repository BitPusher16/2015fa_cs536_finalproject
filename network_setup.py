#!/usr/bin/python

from mininet.log import setLogLevel
from mininet.node import OVSKernelSwitch # , KernelSwitch
from mininet.cli import CLI
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.link import TCLink
from mininet.node import CPULimitedHost
from mininet.util import pmonitor
from signal import SIGINT
from time import time
from time import sleep
import os

class SingleSwitchTopo(Topo):
    #Build the topology here.
    def __init__(self, n=4, ** opts):

        bwth = 10 # Mbits/sec;
        dly = '20ms'
        qsize = 90

        Topo.__init__(self, ** opts)
        
        switch1 = self.addSwitch('s1')
        switch2 = self.addSwitch('s2')

        self.addLink(switch1, switch2, bw=bwth, delay=dly, max_queue_size=qsize)

        host = self.addHost('rs1')  # reno server 1 ("one", not "L");
        self.addLink(host, switch1, bw=bwth, delay=dly)

        host = self.addHost('rc1')  # reno client 1;
        self.addLink(host, switch2, bw=bwth, delay=dly)

        host = self.addHost('vs1')  # vegas server 1;
        self.addLink(host, switch1, bw=bwth, delay = dly)

        host = self.addHost('vc1')  # vegas client 1;
        self.addLink(host, switch2, bw=bwth, delay = dly)

def test():
    os.system('mn -c') # -c means clean and exit;
    switch = OVSKernelSwitch # what is OVSKernelSwitch? should we change it?
    topo = SingleSwitchTopo(0)
    network = Mininet(topo=topo,host=CPULimitedHost,link=TCLink,switch=switch)
    network.start()

    reno_serv = network.get('rs1')
    vegas_serv = network.get('vs1')

    reno_serv.cmd('./server 3000 reno sreno.log 5000 20 &')
    vegas_serv.cmd('./server 3001 vegas svegas.log 5000 0 &')

    reno_cl = network.get('rc1')
    vegas_cl = network.get('vc1')

    reno_cl.cmd('./client %s:3000 file_100m.txt &' % reno_serv.IP())            
    vegas_cl.cmd('./client %s:3001 file_100m.txt &' % vegas_serv.IP())
    
    #sleep(50)
    rcid = int( reno_cl.cmd('echo $!') )
    vcid = int( vegas_cl.cmd('echo $!') )
    reno_cl.cmd('wait', rcid)
    vegas_cl.cmd('wait', vcid)

    # enable this to run CLI commands interactively;
    # suggest using "xterm rs1", etc.
    #CLI(network)

    # alternatively, issue scripted commands to the hosts;

    # stop network;
    network.stop()
    #############################
    # compute throughput per interval
    os.system('./compute_throughput sreno.log rs1_tp.log 4000000')
    os.system('./compute_throughput svegas.log vs1_tp.log 4000000')

    # plot throughput
    os.system('gnuplot plot_two.p')
    #############################
    # merge throughputs
    os.system('./merge_throughputs rs1_tp.log vs1_tp.log fairness.log')

    # plot throughput fairness
    os.system('gnuplot plot_fairness.p')
    #############################
    # compute average window size per interval
    os.system('./compute_cwnd sreno.log rs1_wn.log 1000000')
    os.system('./compute_cwnd svegas.log vs1_wn.log 1000000')
    
    # merge average windows sizes per interval
    os.system('./merge_cwnds rs1_wn.log vs1_wn.log merged_cwnds.log')

    # plot average windows sizes 
    os.system('gnuplot plot_cwnds.p')
    #############################
    # compute window size and RTT
    os.system('./compute_win_rtt sreno.log rs_win.log')
    os.system('./compute_win_rtt svegas.log vs_win.log')
    
    # plot window (plot all, not average)
    os.system('gnuplot plot_win.p')

    # plot RTT
    os.system('gnuplot plot_rtt.p')

if __name__ == '__main__':
    setLogLevel( 'info' )
    test()