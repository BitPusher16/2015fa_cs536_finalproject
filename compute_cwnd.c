/**********
* compute_cwnd.c
*
* This program takes in a server log file and computes average cwnd data points.
* Here is an example of expected log input:
*
* log.txt
* <sys_time> <time> <send_cwnd> <rtt> <total_bytes_transferred>
* 33333343 23 4 2 1060
* 33333348 48 4 2 1590
* 33333390 90 4 3 2130
*
* Here is an example of output:
* <sys_interval_start> <interval_start (micro-secs)> <average_cwnd>
* 33333343 0 44
* 33334343 1000 80
* 33335343 2000 92
* 33336343 3000 103
*
* The interpretation of this output is that in the first 1000 usecs (0-999),
* the server had an average congestion window of size 44 bytes.
*
**********/

#include <stdio.h>
#include <string.h>         // strlen();

#define BUFSIZE 1024
//#define BUFSIZE 530

int main(int argc, char *argv[]){

    // parse command line arguments;
    if(argc != 4){
        printf("usage: %s <infile> <outfile> <interval>\n", argv[0]);
        return 0;
    }
    char infile[BUFSIZE];
    strcpy(infile, argv[1]);
    char outfile[BUFSIZE];
    strcpy(outfile, argv[2]);
    int interval = atoi(argv[3]);
    printf("%s %s %d\n", infile, outfile, interval);

    // open infile;
    FILE *in;
    in = fopen(infile, "r");

    // open outfile;
    FILE *out;
    out = fopen(outfile, "w");

    // get very first sys_time;
    // then rewind infile;
    int read_status;
    unsigned long init_sys_time;
    unsigned long sys_time, time;
    unsigned int snd_cwnd, rtt; //
    int bytes;
    read_status = fscanf(
        in, "%lu %lu %u %u %d", &sys_time, &time, &snd_cwnd, &rtt, &bytes
    );
    init_sys_time = sys_time;
    rewind(in);

    // read infile and compute datapoints for throughput;
    unsigned long curr_intv, prev_intv, ttl_cwnd_intv, avg_cwnd_intv;
    prev_intv = 0;
    ttl_cwnd_intv = 0; // total of all congestion wnd values in current intv;
    avg_cwnd_intv = 0; // average congestion wnd value for current intv;
    int cwnds = 0; // congestion windows counted in current interval;
    while(1){
        read_status = fscanf(
            in, "%lu %lu %u %u %d", &sys_time, &time, &snd_cwnd, &rtt, &bytes
        );

        if(read_status != EOF){
            //printf("time:%u  bytes:%u\n", time, bytes);
            // compute which interval the current log entry falls in;
            curr_intv = time / interval;
            // if we have started a new interval:
            if(curr_intv > prev_intv){
                // write out stats for previous interval;
                if(cwnds > 0){
                    avg_cwnd_intv = ttl_cwnd_intv / cwnds;
                }
                else{
                    avg_cwnd_intv = 0;
                }
                fprintf(out, "%lu %lu %lu\n", 
                    init_sys_time + prev_intv * (unsigned long)interval,
                    prev_intv * (unsigned long)interval, 
                    avg_cwnd_intv
                );

                // write out stats for all missing intervals;
                prev_intv++;
                while(curr_intv > prev_intv){
                    fprintf(out, "%lu %lu %lu\n", 
                        init_sys_time + prev_intv * (unsigned long)interval,
                        prev_intv * (unsigned long)interval, 
                        (unsigned long)0
                    );
                    prev_intv++;
                }

                // set up new interval;
                ttl_cwnd_intv = 0;
                cwnds = 0;
            }
            // update stats for current interval;
            ttl_cwnd_intv += (unsigned long)snd_cwnd;
            cwnds++;
        }
        else{
            break;
        }
    }
    // write out stats for final interval here...
    if(cwnds > 0){
        avg_cwnd_intv = ttl_cwnd_intv / cwnds;
    }
    else{
        avg_cwnd_intv = 0;
    }
    fprintf(out, "%lu %lu %lu\n", 
        init_sys_time + prev_intv * interval,
        prev_intv * interval, 
        avg_cwnd_intv
    );

    // close infile, outfile;
    fclose(in);
    fclose(out);

}
