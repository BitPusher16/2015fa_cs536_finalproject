/**********
* merge_throughputs.c
*
* This program accepts two throughput log files and merges them.
* Expected format for each input log is:
* <system_timestamp> <program_timestamp> <interval_bytes>
*
* Output has format:
* <program_timestamp> <log1_bytes> <log2_bytes>
*
* CAUTION: This program assumes that both throughput log files
* have matching interval lengths.
**********/

#include <stdio.h>
#include <string.h>

#define BUFSIZE 200

int main(int argc, char *argv[]){

    //parse command line arguments;
    if(argc != 4){
        printf("usage: %s <inlog_1> <inlog_2> <outlog>\n", argv[0]);
        return 0;
    }
    char inlog_1[BUFSIZE];
    strcpy(inlog_1, argv[1]);
    char inlog_2[BUFSIZE];
    strcpy(inlog_2, argv[2]);
    char outlog[BUFSIZE];
    strcpy(outlog, argv[3]);
    printf("%s %s %s\n", inlog_1, inlog_2, outlog);

    // open inlog_1;
    FILE *in_1;
    in_1 = fopen(inlog_1, "r");

    // open inlog_2;
    FILE *in_2;
    in_2 = fopen(inlog_2, "r");

    // open outlog;
    FILE *out;
    out = fopen(outlog, "w");

    // sanity check;
    if(in_1 == NULL || in_2 == NULL || out == NULL){
        printf("failed to open file\n");
        return 1;
    }

    int read_status_1, read_status_2;
    unsigned long sys_ts_1, sys_ts_2, ts_1, ts_2;
    int bytes_1, bytes_2;
    while(1){
        read_status_1 = fscanf(in_1, "%lu %lu %d", &sys_ts_1, &ts_1, &bytes_1);
        read_status_2 = fscanf(in_2, "%lu %lu %d", &sys_ts_2, &ts_2, &bytes_2);
        if(read_status_1 != EOF && read_status_2 != EOF){
            fprintf(out, "%lu %d %d\n", ts_1, bytes_1, bytes_2);
        }
        else{
            break;
        }
    }

    if(read_status_1 != EOF && read_status_2 == EOF){
        // file 1 has remaining content;
        // add line from previous read, then process remainder of file 1;
        fprintf(out, "%lu %d %d\n", ts_1, bytes_1, 0);
        while(1){
            read_status_1 = fscanf(
                in_1, "%lu %lu %d", &sys_ts_1, &ts_1, &bytes_1
            );
            if(read_status_1 != EOF){
                fprintf(out, "%lu %d %d\n", ts_1, bytes_1, 0);
            }
            else{
                break;
            }
        }
    }
    else if(read_status_1 == EOF && read_status_2 != EOF){
        // file 2 has remaining content;
        // add line from previous read, then process remainder of file 2;
        fprintf(out, "%lu %d %d\n", ts_1, 0, bytes_2);
        while(1){
            read_status_2 = fscanf(
                in_2, "%lu %lu %d", &sys_ts_2, &ts_2, &bytes_2
            );
            if(read_status_2 != EOF){
                fprintf(out, "%lu %d %d\n", ts_2, 0, bytes_2);
            }
            else{
                break;
            }
        }
    }
    
    // close file handles;
    fclose(in_1);
    fclose(in_2);
    fclose(out);

    return 0;
}
