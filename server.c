/**********
*
* server.c
*
* accepts a connection from client;
* receives file request from client;
* sends file to client;
*   while sending, writes out log;
*
* we attempt to write a log line after every packet;
* log fields are:
*   <system_time (usec)>
*   <time_since_start (usec)> 
*   <snd_cwnd> 
*   <RTT (usec)> 
*   <total_bytes_transferred>
*
* NOTE: total bytes transferred is in bytes, not bits;
*
* NOTE: server accepts delay as a command line argument;
*       this delay is specified in seconds, not microseconds;
* 
**********/

/*
#include <stdio.h>          // printf();
#include <string.h>         // strlen();
#include <sys/socket.h>     // socket();
#include <netinet/in.h>     // struct sockaddr_in;
#include <netdb.h>          // gethostbyname();
#include <stdlib.h>         // exit();
#include <errno.h>          // errno;

#include <linux/tcp.h>      // TCP_CONGESTION;
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

// be careful modifying these headers;
// some of the headers in the top half conflict with
// headers in the bottom half;


//#define BUFSIZE 1024  // if we set this to less than MSS 
                        // it should guarantee one status per pkt
#define BUFSIZE 530

// computes the difference between two timevals;
// returns answer in microseconds;
unsigned long t_diff(struct timeval *t1, struct timeval *t2){
    return (
        (unsigned long)(t2->tv_sec - t1->tv_sec) * 1e6 
        + (unsigned long)(t2->tv_usec - t1->tv_usec)
    );
}

int main(int argc, char *argv[]){

    //parse command line arguments;
    if(argc != 6){
        printf("usage: %s <port> <protocol> <log> <bytes_per_log_line> "
            "<delay>\n", argv[0]);
        return 0;
    }
    int port = atoi(argv[1]);
    char protocol[BUFSIZE];
    strcpy(protocol, argv[2]);
    char log[BUFSIZE];
    strcpy(log, argv[3]);
    int bpll = atoi(argv[4]);
    int delay = atoi(argv[5]);
    printf("%d %s %s %d %d\n", port, protocol, log, bpll, delay);

    // create socket;
    int fd;
    if((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        perror("cannot create socket");
        return 0;
    }

    // allow immediate reuse of local port;
    int sockoptval = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &sockoptval, sizeof(int));

    // bind socket to local port;
    struct sockaddr_in servaddr;
    memset((char *)&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);        // specify server port;
    if(bind(fd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0){
        perror("bind failed");
        return 0;
    }

    // set socket to listen;
    if(listen(fd, 5) < 0){
        perror("listen failed");
        exit(1);
    }

    // accept client connection request;
    // (we use a while loop to wait for the request because other
    // system events can also cause accept() to return);
    int rqst;
    socklen_t alen;
    struct sockaddr_in clientaddr;
    while((rqst = accept(fd, (struct sockaddr *)&clientaddr, &alen)) < 0){
        if((errno != ECHILD) && (errno != ERESTART) && (errno != EINTR)){
            perror("accept failed");
            exit(1);
        }
    }
    printf("accepted connection\n");

    // set congestion algorithm on request socket;
    // http://sgros.blogspot.com/2012/12/controlling-
    //      which-congestion-control.html
    int optlen = strlen(protocol);
    if(setsockopt(rqst, IPPROTO_TCP, TCP_CONGESTION, protocol, optlen) < 0){
        perror("set protocol failed");
        return 1;
    }

    // check send MSS, receive MSS
    struct tcp_info tcpi;
    int tcpi_length = sizeof(tcpi);
    if(getsockopt(rqst, SOL_TCP, TCP_INFO, (void *)&tcpi, &tcpi_length) == 0){
        printf(
            "tcpi_snd_mss:%d tcpi_rcv_mss:%d\n", 
            tcpi.tcpi_snd_mss, tcpi.tcpi_rcv_mss
        );
    }

    // read requested file name from client;
    int nbytes;
    char file_name[BUFSIZE];
    memset(file_name, 0, BUFSIZE);
    nbytes = read(rqst, file_name, BUFSIZE);
    printf("client requested file: %s\n", file_name);

    // open requested file;
    FILE *fp;
    fp = fopen(file_name, "r");
    if(fp == NULL){
        printf("could not open requested file\n");
        close(fd);
        return(0);
    }

    // open log file;
    FILE *lp;
    lp = fopen(log, "w");

    // set up structs for timekeeping;
    // start time;
    struct timeval t1, t2, tzero;
    tzero.tv_sec = 0;
    tzero.tv_usec = 0;
    gettimeofday(&t1, NULL);

    // if delay is specified, sleep;
    sleep(delay);

    // write entire file to socket;
    int fbytes, bytes_sent;
    bytes_sent = 0;
    //char outbuf[BUFSIZE];
    char *outbuf = malloc(bpll + 1);
    while(1){
        // copy bytes from file to outbuffer;
        // note:    fgetc() gets a single character;
        //          fgets() stops at '\n' and EOF;
        //          fscanf() stops at whitespace;
        //          use fread();

        //fbytes = fread(outbuf, 1, BUFSIZE, fp);
        fbytes = fread(outbuf, 1, bpll, fp);
        if(fbytes > 0){
            // write to client
            nbytes = write(rqst, outbuf, fbytes);
            bytes_sent += nbytes;
            gettimeofday(&t2, NULL);
            if(getsockopt(rqst, SOL_TCP, TCP_INFO, 
                (void *)&tcpi, &tcpi_length) == 0)
            {
                //printf(
                fprintf(lp,
                    //"%.8f %u %u %d\n", 
                    //"%u %u %u %d\n", 
                    "%lu %lu %u %u %d\n", 
                    t_diff(&tzero, &t2),    // microseconds;
                    t_diff(&t1, &t2),       // microseconds;
                    tcpi.tcpi_snd_cwnd,     // multiples of MSS?
                    tcpi.tcpi_rtt,          // microseconds;
                    bytes_sent
                );
            }
        }
        else{
            // close socket to client;
            close(rqst);
            break;
        }
    }


    // close listening socket, in-file, log file;
    close(fd);
    fclose(fp);
    //close(fp);
    fclose(lp);

    return 0;
}
