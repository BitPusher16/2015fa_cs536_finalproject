/**********
* compute_throughput.c
*
* This program takes in a server log file and computes throughput data points.
* Here is an example of expected log input:
*
* log.txt
* <sys_time> <time> <send_cwnd> <rtt> <total_bytes_transferred>
* 33333343 23 4 2 1060
* 33333348 48 4 2 1590
* 33333390 90 4 3 2130
*
* Here is an example of output:
* <sys_interval_start> <interval_start (micro-secs)> <throughput>
* 33333343 0 1060
* 33334343 1000 1060
* 33335343 2000 1590
* 33336343 3000 530
*
* The interpretation of this output is that in the first 1000 usecs (0-999),
* the server transferred 1060 bytes.
* Thus, throughput for the first interval is 1060 B/1000 usec, or 1060 B/1 msec.
*
* NOTE: throughput is in bytes, not bits;
*
* HINT: to sum results of output file, use this bash command:
* $ cat rs_int.log | awk '{sum+=$3 ; print $0} END{print "sum=",sum}
*
**********/

#include <stdio.h>
#include <string.h>         // strlen();

#define BUFSIZE 1024
//#define BUFSIZE 530

int main(int argc, char *argv[]){

    // parse command line arguments;
    if(argc != 4){
        printf("usage: %s <infile> <outfile> <interval>\n", argv[0]);
        return 0;
    }
    char infile[BUFSIZE];
    strcpy(infile, argv[1]);
    char outfile[BUFSIZE];
    strcpy(outfile, argv[2]);
    int interval = atoi(argv[3]);
    printf("%s %s %d\n", infile, outfile, interval);

    // open infile;
    FILE *in;
    in = fopen(infile, "r");

    // open outfile;
    FILE *out;
    out = fopen(outfile, "w");

    // get very first sys_time;
    // then rewind infile;
    int read_status;
    unsigned long init_sys_time;
    unsigned long sys_time, time;
    unsigned int snd_cwnd, rtt; //
    int bytes;
    read_status = fscanf(
        in, "%lu %lu %u %u %d", &sys_time, &time, &snd_cwnd, &rtt, &bytes
    );
    init_sys_time = sys_time;
    rewind(in);

    // read infile and compute datapoints for throughput;
    unsigned long curr_intv, prev_intv, bytes_intv, bytes_offset;
    prev_intv = 0;
    bytes_intv = 0;
    bytes_offset = 0;
    while(1){
        read_status = fscanf(
            in, "%lu %lu %u %u %d", &sys_time, &time, &snd_cwnd, &rtt, &bytes
        );
        if(read_status != EOF){
            //printf("time:%u  bytes:%u\n", time, bytes);
            // compute which interval the current log entry falls in;
            curr_intv = time / interval;
            // if we have started a new interval:
            if(curr_intv > prev_intv){
                // write out stats for previous interval;
                fprintf(out, "%lu %lu %lu\n", 
                    init_sys_time + prev_intv * (unsigned long)interval,
                    prev_intv * (unsigned long)interval, 
                    bytes_intv
                );

                // write out stats for all missing intervals;
                prev_intv++;
                while(curr_intv > prev_intv){
                    fprintf(out, "%lu %lu %lu\n", 
                        init_sys_time + prev_intv * (unsigned long)interval,
                        prev_intv * (unsigned long)interval, 
                        (unsigned long)0
                    );
                    prev_intv++;
                }

                // set up new interval;
                bytes_intv = 0;
            }
            // update stats for current interval;
            bytes_intv += bytes - bytes_offset;
            bytes_offset = bytes;

        }
        else{
            break;
        }
    }
    // write out stats for final interval here...
    fprintf(out, "%lu %lu %lu\n", 
        init_sys_time + prev_intv * interval,
        prev_intv * interval, 
        bytes_intv
    );

    // close infile, outfile;
    fclose(in);
    fclose(out);

}
